FROM php:7.3.6-fpm
WORKDIR /home/src/bayes-classification
COPY . /home/src/bayes-classification
CMD ["php","-S","0.0.0.0:8000"]
EXPOSE 8000